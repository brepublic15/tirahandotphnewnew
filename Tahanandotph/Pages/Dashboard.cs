﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Nexmo.Api;

namespace Tahanandotph.Pages
{
    [Activity(Theme = "@style/Theme.AppCompat.Light.NoActionBar", MainLauncher = false)]
    public class Dashboard : Activity
    {
        Button paybtn;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.dashboard);

            paybtn = FindViewById<Button>(Resource.Id.pay_btn);

            paybtn.Click += Paybtn_Click;
        }

        private async void Paybtn_Click(object sender, EventArgs e)
        {
            await SendSMS();
        }

        private Task SendSMS()
        {
            var datePaid = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
            var amount = "P21,000.00";
            var message = $"You have paid monthly due amounting to {amount} to Condo Cities PH on {datePaid}";
            var contact = "639268580286";

            //There is credit limit of 2USD so uncomment these codes if you are done testing
            //using (var client = new HttpClient())
            //{
            //    var values = new List<KeyValuePair<string, string>>();
            //    values.Add(new KeyValuePair<string, string>("from", "Tahanan"));
            //    values.Add(new KeyValuePair<string, string>("text", message));
            //    values.Add(new KeyValuePair<string, string>("to", contact));
            //    values.Add(new KeyValuePair<string, string>("api_key", "f8978497"));
            //    values.Add(new KeyValuePair<string, string>("api_secret", "EwJcucg3FfCS2q3Z"));

            //    var content = new FormUrlEncodedContent(values);

            //    client.PostAsync("https://rest.nexmo.com/sms/json", content).Wait();
            //}

            paybtn.Text = "Paid";
            paybtn.Enabled = false;
            paybtn.SetBackgroundResource(Resource.Drawable.backgroundpaid);

            return Task.FromResult(0);
        }
    }
}