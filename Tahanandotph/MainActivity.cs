﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Tahanandotph.Pages
{
    [Activity(Theme = "@style/Theme.AppCompat.Light.NoActionBar", MainLauncher = true)]
    public class Login : Activity
    {
        public Button login_btn;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            // Create your application here
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.login);

            login_btn = FindViewById<Button>(Resource.Id.login_btn);
            login_btn.Click += Btn_Submit_Click;


        }
        void Btn_Submit_Click(object sender, EventArgs e)
        {
            Intent dashboard = new Intent(this, typeof(Dashboard));
            StartActivity(dashboard);
        }
    }
}